import cv2
import numpy as np

frame_width = 640
frame_height = 480
cap = cv2.VideoCapture(0)

cap.set(3, frame_width)
cap.set(4, frame_height)


def empty():
    pass


cv2.namedWindow("Parameters")
cv2.resizeWindow("Parameters", 640, 240)
cv2.createTrackbar("Threshold1", "Parameters", 23, 255, empty)
cv2.createTrackbar("Threshold2", "Parameters", 22, 255, empty)

while True:
    ret, frame = cap.read()

    # blurring image
    blurred_img = cv2.GaussianBlur(frame, (7, 7), 1)
    # converting to gray scale
    gray_img = cv2.cvtColor(blurred_img, cv2.COLOR_BGR2GRAY)

    threshold1 = cv2.getTrackbarPos('Threshold1', "Parameters")
    threshold2 = cv2.getTrackbarPos('Threshold2', "Parameters")

    canny_img = cv2.Canny(gray_img, threshold1, threshold2)

    stock = np.concatenate((frame, gray_img, canny_img), axis=1)

    cv2.imshow("Current frame", stock)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


