__author__ = 'Krystian Banach'
__version__ = 'Windows'
__file__ = 'frame_capturing.py'
__description__ = 'Modul do wyciaganie klatek z webcam albo pliku video .mp4'
__usage__webcam__ = 'python frame_capturing.py'
__usage__file__ = 'python frame_capturing.py -v path\.mp4'

import cv2
import uuid
import argparse
import os
from datetime import datetime, date
import time

# take arguments from terminal
parser = argparse.ArgumentParser()
parser.add_argument('-v', "--video", metavar='', help="Path to video file (optional)")
args = vars(parser.parse_args())

# webcam
if not args.get("video", False):
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
# video file
else:
    cap = cv2.VideoCapture(args["video"])

# creating folder for captured images
cwd = os.getcwd()
current_time = datetime.now()
new_dir_path = cwd + f'\captured_frames_{date.today()}_{current_time.hour}-{current_time.minute}'
try:
    os.mkdir(new_dir_path)
except FileExistsError:
    print("Wait until next minute")

frame_num = 1
cnt = 0

start = time.time()

while True:
    # take frame
    ret, frame = cap.read()
        
    # if video is over
    if frame is None:
        break
    
    # write captured frame
    image_name = new_dir_path + f"\{str(uuid.uuid1())}.jpg"
    cv2.imwrite(image_name, frame)
    
    # dipslay
    cv2.imshow('Current frame', frame)
    
    # display info about capturing
    if cnt == 50:
        print("[FrameCapture] Capturing...")
        print(f"[FrameCapture] {frame_num} captured.")
        cnt = 0        
    cnt += 1
    frame_num += 1
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
cap.release()
cv2.destroyAllWindows()
   
end = time.time()
cap_time = end - start

print(f"""   
[FrameCapture] Capturing complete.
    Capturing time: {cap_time}
    Amount of captured frames: {frame_num}
    FPS: {frame_num / cap_time}
    """)
